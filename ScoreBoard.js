const { html, Component, render } = htmPreact;

const DATE_F = new Intl.DateTimeFormat("default", {
  year: "numeric",
  month: "short",
  day: "numeric",
  hour: "numeric",
  minute: "numeric"
});
const TIME_F = new Intl.DateTimeFormat("default", {
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
  milisecond: "numeric",
  fractionalSecondDigits: 3
});

class ScoreBoard extends Component {
  render({ gameHistory, filter }, state) {
    const games = gameHistory.filter(filter);

    return html`<table class="scoreboard">
        <style>table.scoreboard {
          --c: #999;
          margin: 0 auto;
          color: var(--c);
        }        
        .scoreboard td:not(:last-child) {
          /*border-right: 1px solid var(--c);*/
        }
        .scoreboard td {
          padding: 0 .25em;
          vertical-align: baseline;
        }</style>
      ${
        games.length
          ? games.map(
              game =>
                html`<tr>
            <td>${new Date(game.duration).toISOString().substr(14, 9)}</>
            <td class="subtle">${DATE_F.format(game.startTime)}</>
          </>`
            )
          : html`<span class="subtle">∅</>`
      }

    </>`;
  }
}

export default ScoreBoard;
