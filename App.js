/* global htmPreact */
const { html, Component, render } = htmPreact;

import { Game, GameStatus } from "./Game.js";
import ScoreBoard from "./ScoreBoard.js";
import StatusTitle from "./StatusTitle.js";

function uuidv4() {
  // https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript#2117523
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}

const HISTORY_LENGTH = 100;

const SMALLBOARDCONFIG = "10;10;10";
const BIGBOARDCONFIG = "30;20;60";

class App extends Component {
  version = 1;

  constructor() {
    super();
    this.state = {
      gameId: undefined,
      gameStatus: 0,
      gameHistory: null,
      boardConfig: SMALLBOARDCONFIG,
      canvasWidth: 300,
      canvasHeight: 300,
      warnMessages: []
    };

    new Promise(async (resolve, reject) => {
      resolve(await getStorage());
    })
      .then(storage => {
        this.storage = storage;
      })
      .catch(e => {
        this.storage = { load: function() {}, save: function() {} };
        this.setState({
          warnMessages: [
            ...this.state.warnMessages,
            { message: "💾", data: e.message }
          ]
        });
      })
      .finally(() => {
        const previousVersion = this.storage.load("version") || 0;
        if (previousVersion < 1) {
          // reset history for App version lower than 1
          this.setState({ gameHistory: [] });
        }
        this.storage.save("version", this.version);

        this.setState({ gameHistory: this.storage.load("history") || [] });
      });

    this.setState({ gameId: uuidv4() });
  }

  async componentDidMount() {}

  onGameUpdate(updateGame) {
    let { currentGame = {}, gameHistory } = this.state;

    if (
      // Did the game just end?
      currentGame.status === GameStatus.RUNNING &&
      [GameStatus.WON, GameStatus.LOST].includes(updateGame.status)
    ) {
      gameHistory.push(updateGame);
      gameHistory = cleanHistory(gameHistory);
    }

    this.storage.save("history", gameHistory);

    this.setState({
      currentGame: updateGame,
      gameHistory
    });
  }

  render(
    props,
    {
      currentGame = {},
      gameId,
      gameHistory,
      warnMessages,
      boardConfig,
      canvasWidth,
      canvasHeight
    }
  ) {
    // todo: clean up
    const [w, h] = boardConfig.split(";");
    const currentGameSize = w * h;
    const filterScores = game => {
      const [w, h] = game.boardConfig.split(";");
      return w * h === currentGameSize;
    };

    return html`
      <div class="App">
        <header>
          <div class="controls">
            <button
              disabled=${!gameHistory}
              class=${boardConfig === SMALLBOARDCONFIG && "current-game-config"}
              onclick=${() => {
                this.setState({
                  gameId: uuidv4(),
                  boardConfig: SMALLBOARDCONFIG,
                  canvasWidth: 300,
                  canvasHeight: 300
                });
              }}
            >small game<div class="subtle">${String(SMALLBOARDCONFIG)
              .replace(";", "x")
              .replace(";", "@")}</></>
            <button
              disabled=${!gameHistory}
              class=${boardConfig === BIGBOARDCONFIG && "current-game-config"}
              onclick=${() => {
                this.setState({
                  gameId: uuidv4(),
                  boardConfig: BIGBOARDCONFIG,
                  canvasWidth: 900,
                  canvasHeight: 600
                });
              }}
            >big game<div class="subtle">${String(BIGBOARDCONFIG)
              .replace(";", "x")
              .replace(";", "@")}</></>
          </>
          <${StatusTitle} 
            status=${currentGame.status} 
            startTime=${currentGame.startTime} 
          />
        </>
        <div id="Game">
          <${Game}
            width=${canvasWidth}
            height=${canvasHeight}
            boardConfig=${boardConfig}
            gameId=${gameId}
            onGameUpdate=${gameState => {
              this.onGameUpdate(gameState);
            }}
          />
        </div>
        <span>${boardConfig === SMALLBOARDCONFIG ? "small " : "big "}scores</>
        <${ScoreBoard} gameHistory=${gameHistory || []} filter=${filterScores}/>
        <footer>
          <div>${warnMessages.map(
            w => html`<span class="toast">${w.message}<span>${w.data}</></>`
          )}</div>
        </footer>
      </div>
    `;
  }
}

// ========

function cleanHistory(history, game) {
  let times = history
    // erase pre-v1.0
    .filter(game => game.boardConfig)
    .filter(game => game.status === GameStatus.WON)
    .map((game, idx) => [idx, game.duration])
    .sort((a, b) => a[1] - b[1])
    .splice(0, HISTORY_LENGTH);

  return times.map(t => history[t[0]]);
}

// =========

render(
  html`<div>
    <style>
      button {
        font: inherit;
        background: #ffffff06;
        color: #eee8;
        border: unset;
        padding: 0.25em;
        cursor: pointer;
      }
      button::before {
        content: "";
        position: absolute;
        display: block;
        border-left: 1.3em solid #ffffff05;
        border-bottom: 1.3em solid transparent;
      }
      button:hover {
        color: #eee;
      }

      button.current-game-config::before {
        border-left-color: #eee8;
      }

      .subtle {
        font-size: x-small;
        opacity: 0.5;
      }

      header {
        padding: 0.25em;
      }

      header .controls {
        margin: 1em 0;
        display: flex;
      }

      header .controls > button {
        margin: 0 0.5em;
        flex: 1 0 auto;
      }

      #Game {
        text-align: center;
        margin: 2em 0;
      }

      footer {
        position: absolute;
        bottom: 0;
        padding: 1%;
        width: 98%;
      }
    </>
    <${App} />
  </>`,
  document.body
);

// ========

async function getStorage() {
  if (!isLocalStorageAvailable())
    return Promise.reject(new Error("localStorage not available"));

  return Promise.resolve({
    load: function(key) {
      try {
        return JSON.parse(localStorage.getItem(key));
      } catch (e) {
        console.error(e);
        return undefined;
      }
    },
    save: function(key, value) {
      return localStorage.setItem(key, JSON.stringify(value));
    }
  });
}

function isLocalStorageAvailable() {
  // https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
  let storage;
  try {
    storage = window.localStorage;
    var x = "__storage_detection_test__";
    storage.setItem(x, x);
    storage.removeItem(x);
    return true;
  } catch (e) {
    return (
      e instanceof DOMException &&
      // everything except Firefox
      (e.code === 22 ||
        // Firefox
        e.code === 1014 ||
        // test name field too, because code might not be present
        // everything except Firefox
        e.name === "QuotaExceededError" ||
        // Firefox
        e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
      // acknowledge QuotaExceededError only if there's something already stored
      storage &&
      storage.length !== 0
    );
  }
}
