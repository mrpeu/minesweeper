const { html, Component } = htmPreact;

import { GameStatus } from "./Game.js";

// todo: clean up
const getGameStatusString = statusId => {
  for (let statusStr in GameStatus) {
    if (GameStatus[statusStr] === statusId) return statusStr;
  }
};

const INTERVAL = 1000;

class StatusTitle extends Component {
  interval = null;

  componentDidUpdate({ status }) {
    switch (status) {
      case GameStatus.RUNNING:
        // start it
        if (!this.interval)
          this.interval = setInterval(
            /* force render */
            () => this.setState({}),
            INTERVAL
          );
        break;

      default:
        // stop it
        clearInterval(this.interval);
        break;
    }
  }

  render({ startTime, status }, state) {
    const timeString = `${~~((Date.now() - startTime) / 1000)} s.`;

    const message =
      status !== GameStatus.RUNNING && getGameStatusString(status);

    return html`<div class="status-title">
      <style>
        .status-title {
          text-align: center;
          height: 1em;
          margin-top: 2em;
        }
        .status-title span {
          margin: 0 .5em;
        }
        .status-title .time-string { 
          font-size: smaller;
        }
      </>
      <div>
        ${message}
        <span class="subtle time-string">${message && "in "}${timeString}</>
      </>
    </>`;
  }
}

export default StatusTitle;
