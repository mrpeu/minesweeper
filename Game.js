const { html, Component, render } = htmPreact;
import { drawBoard } from "./draw.js";

const GameStatus = {
  STOPPED: 0,
  RUNNING: 1,
  WON: 2,
  LOST: 3
};

class Game extends Component {
  constructor() {
    super();

    this.state = {
      ctx: null,
      gameId: null,
      width: null,
      height: null,
      nbMines: null,
      boardWidth: null,
      boardHeight: null,
      startTime: null,
      duration: null,
      status: null,

      // boardMap: [...]
      // 0: nothing
      // 1-8: nb surrounding bombs
      // 9: bomb
      boardMap: null,

      // boardConfig: [...]
      // 0: pristine
      // 1: revealed
      // 2: flagged
      boardState: null
    };

    this.isDirty = false;
  }

  componentDidMount() {
    this.state.ctx = this.base.getContext("2d");

    // 🤷‍ "Initially the canvas positioning point between the physical pixels."
    // state.ctx.translate(0.5, 0.5);
  }

  // detect new gameId
  componentDidUpdate() {
    // nothing to do
    if (!this.props.gameId) return;

    this.markAsDirty();

    if (this.props.gameId === this.state.gameId) return;

    // otherwise, a new gameId has been provided
    this.startGame(this.props.gameId);
  }

  markAsDirty(isDirty = true) {
    this.isDirty = isDirty;
  }

  render(props, state) {
    // console.log(
    //   "state:",
    //   state.boardState.reduce(
    //     (acc, cur, idx) => `${acc} ${idx % state.boardWidth === 0 ? "\n" : ""}`,
    //     ""
    //   )
    // );

    return html`
      <canvas
        style="min-width: 10em; max-width: 90%"
        width=${state.width}
        height=${state.height}
        onclick=${ev => this.onclick(ev)}
        oncontextmenu=${ev => {
          ev.preventDefault();
          this.onclick(ev, true);
        }}
      />
    `;
  }

  // ========
  // Game stuff

  startGame() {
    const { props } = this;

    console.assert(props.gameId, "gameId needed to startTime a game!");

    // Is there already a Game running (& isn't stopped)?
    if (this.state.startTime > 0 && !(this.state.duration > 0)) {
      this.endGame(GameStatus.STOPPED);
    }

    // Gather configurations from props
    const width = Number.parseInt(props.width);
    const height = Number.parseInt(props.height);
    const [boardWidth, boardHeight, nbMines] = (props.boardConfig || "10;10;32")
      .split(";")
      .map(Number);

    this.state = {
      ...this.state,
      gameId: this.props.gameId,
      width,
      height,
      boardWidth: boardWidth,
      boardHeight: boardHeight,
      nbMines: nbMines,
      // 0: nothing
      // 1-8: nb surrounding bombs
      // 9: bomb
      boardMap: generateBoardMap(boardWidth, boardHeight, nbMines),
      // 0: pristine
      // 1: revealed
      // 2: flagged
      boardState: new Array(boardWidth * boardHeight).fill(0),
      startTime: Date.now(),
      duration: -1,
      status: GameStatus.RUNNING
    };

    // DEBUG
    if (this.animationFrameId) {
      console.error(
        "Starting a new game, but here's already a render loop active!"
      );
    }

    this.animationFrameId = requestAnimationFrame(() => {
      this.loopRenderGame();
    });

    this.props.onGameUpdate(this.serialize());
  }

  checkForEndGame() {
    const { boardMap, boardState, nbMines } = this.state;

    let hasMineBoomed = false;
    let nbMinesFlagged = 0;
    let nbUnrevealedCells = 0;

    for (let id = 0; id < boardMap.length; id++) {
      const cellConfig = boardMap[id];
      const cellState = boardState[id];

      if (cellState === 0) nbUnrevealedCells++;

      if (cellConfig === 9) {
        if (cellState === 1) {
          hasMineBoomed = true;
          break;
        } else if (cellState === 2) nbMinesFlagged++;
      }
    }

    // console.log({
    //   hasMineBoomed,
    //   nbMinesFlagged,
    //   nbUnrevealedCells,
    //   nbMines
    // });

    // end of game?
    if (
      // lost
      hasMineBoomed ||
      // win condition 1: all mines have been flagged
      nbMines === nbMinesFlagged ||
      // win condition 1: all but the mines have been flagged
      nbUnrevealedCells == nbMines
    ) {
      const isWon = hasMineBoomed === false;

      return this.endGame(isWon ? GameStatus.WON : GameStatus.LOST);
    }

    // default: no endgame
    return this.props.onGameUpdate(this.serialize());
  }

  endGame(status) {
    console.assert(Number.isInteger(status), "there should be a status");

    const { boardMap, boardState } = this.state;

    // todo boom effect

    boardMap.forEach((cellConfig, id) => {
      if (cellConfig === 9) boardState[id] = 1;
    });

    this.state = {
      ...this.state,
      duration: Date.now() - this.state.startTime,
      status
    };

    cancelAnimationFrame(this.animationFrameId);
    delete this.animationFrameId;

    this.props.onGameUpdate(this.serialize());
  }

  loopRenderGame() {
    if (this.isDirty) {
      this.isDirty = false;
      drawBoard(this.state);
    }
    requestAnimationFrame(() => {
      this.loopRenderGame();
    });
  }

  onclick(ev, isSecondary = false) {
    if (!this.state.gameId) return;

    if (this.state.status !== GameStatus.RUNNING) return;

    const {
      width,
      height,
      boardWidth,
      boardHeight,
      boardMap,
      boardState
    } = this.state;

    const x = ~~(ev.offsetX / (this.base.scrollWidth / boardWidth));
    const y = ~~(ev.offsetY / (this.base.scrollHeight / boardHeight));
    const mapId = x + y * boardWidth;
    const cellConfig = boardMap[mapId];
    const cellState = boardState[mapId];

    switch (cellState) {
      // 0: pristine
      case 0:
        // right click
        if (isSecondary) {
          // flag it
          boardState[mapId] = 2;
          this.markAsDirty();
          break;
        }

        switch (cellConfig) {
          // 0: empty cell
          case 0:
            // update cell's state
            boardState[mapId] = 1;

            // update surrounding cell's state
            getAllEmptySurroundingIndices(
              mapId,
              boardWidth,
              boardHeight,
              boardMap
            ).forEach(id => (boardState[id] = 1));

            break;

          // 1-8: cell surrounded by n bombs
          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
          case 8:
            boardState[mapId] = 1;
            break;

          // 9: cell bomb
          case 9:
            boardState[mapId] = 1;
            break;

          // invalid cell config
          default:
            console.error(
              `Invalid boardState[${x},${y}:${mapId}]`,
              boardState[mapId]
            );
            break;
        }

        this.markAsDirty();
        break;

      // 1: cell already revealed
      case 1:
        break;

      // 2: cell flagged
      case 2:
        // right click
        if (isSecondary) {
          // un-flag it
          boardState[mapId] = 0;
          this.markAsDirty();
          break;
        }
        break;

      default:
        console.error(
          `Invalid boardState[${x},${y}:${mapId}]`,
          boardState[mapId]
        );
        break;
    }

    this.isDirty && this.checkForEndGame();

    // console.log(`${x},${y}:${cellConfig}`);
  }

  serialize() {
    return {
      gameId: this.state.gameId,
      status: this.state.status,
      boardConfig: `${this.state.boardWidth};${this.state.boardHeight};${this.state.nbMines}`,
      startTime: this.state.startTime,
      duration: this.state.duration
    };
  }
}

function generateBoardMap(width, height, nbMines) {
  const availableCells = new Set([...Array(width * height).keys()]);
  const mineList = new Set();

  for (let i = 0, newMine; i < nbMines; i++) {
    newMine = ~~(Math.random() * availableCells.size);
    mineList.add(newMine);
    availableCells.delete(newMine);
  }

  // 0 => no mine
  // 9 => mine
  let boardMap = new Array(width * height).fill(0);

  boardMap.forEach((value, index) => {
    if (mineList.has(index)) {
      boardMap[index] = 9;
      // increment surrounding cells to account for this mine
      const surrounds = getSurroundingIndices(index, width, height);
      surrounds.forEach(i => {
        if (i !== null && boardMap[i] != 9) {
          boardMap[i]++;
        }
      });
    }
  });

  return boardMap;
}

function getSurroundingIndices(index, width, height) {
  const isLeftCol = index % width === 0;
  const isRightCol = index % width === width - 1;
  const isTopRow = index < width;
  const isBottomRow = index >= (height - 1) * width;

  return [
    !isTopRow && !isLeftCol ? index - 1 - width : null,
    !isTopRow ? index + 0 - width : null,
    !isTopRow && !isRightCol ? index + 1 - width : null,
    !isLeftCol ? index - 1 + 0 : null,
    !isRightCol ? index + 1 + 0 : null,
    !isBottomRow && !isLeftCol ? index - 1 + width : null,
    !isBottomRow ? index + 0 + width : null,
    !isBottomRow && !isRightCol ? index + 1 + width : null
  ];
}

function getAllEmptySurroundingIndices(
  index,
  width,
  height,
  boardMap,
  accumulator = new Set()
) {
  getSurroundingIndices(index, width, height).forEach(id => {
    if (id === null || accumulator.has(id) || boardMap[id] === 9) {
      return;
    }

    accumulator.add(id);

    if (boardMap[id] === 0)
      getAllEmptySurroundingIndices(
        id,
        width,
        height,
        boardMap,
        accumulator
      ).forEach(newId => boardMap[id] === 0 && accumulator.add(newId));
  });

  return accumulator;
}

export { Game, GameStatus };
