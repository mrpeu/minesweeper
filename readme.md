#minesweeper #preact #htm #canvas2d #nativejsmodule

# todo

- [x] Project initialisation (preact+htm, Git)
- [x] Core : Game initialisation (Game component, update lifecycle in context of preact)
- [x] Canvas : Grid
- [x] Core : board initialisation (config vs state)
- [x] Core : mines and numbered cells initialisation
- [x] Core : onclick cell detection
- [x] Core : browse grid (stored in a 1d array), find surrounding cells
- [x] Core : trigger new game from preact App "side"
- [x] ReadMe
- [x] Push to GitLab
- [x] Endgame : trigger when all bombs have been flagged
- [x] Endgame : reveal all bombs
- [x] Storage : save&load current game state
- [x] Storage : save history of games
- ~~[x] Storage : import a game~~
- [x] Core : generateMap with a number of bombs (instead of a ratio)
- [x] Core : add a live timer
- [x] Storage : undo "save current game state" as it is kinda useless to resume a game and makes the timing complicated.
- [x] Score : keep the Game's config associated with the saved timings
- [ ] Score : algo for scores incorporating the number of bombs, the grid size, etc
- [x] Deploy : ~~begin? NextJS?~~ GitLab pages
- [ ] Deploy : online scores with pseudonyme / anonymous. If pseudonyme, generate a uid to check serverside if this is still an existing one, otherwise generate a new one with exponent 2, 3, etc
- [x] Core : touch support (kinda implicitly supported by handling "click" and "contextmenu" events)
- [ ] Deploy : versioning and notification of new version

- [ ] Canvas : add some animations
- [ ] Sounds : something

- [ ] Multi : init a server (serverless function + Mongo Cloud?)
- [ ] Multi : show active games
- [ ] Multi : init WebSocket to join others games
