import { GameStatus } from "./Game.js";

function drawBoard({
  ctx,
  width,
  height,
  boardWidth,
  boardHeight,
  boardState,
  boardMap,
  status
}) {
  ctx.save();
  ctx.clearRect(0, 0, width, height);

  // size of a cell, in pixels
  const uX = width / boardWidth;
  const uY = height / boardHeight;

  const fillEmpty = ctx.createRadialGradient(
    uX / 2,
    uY / 2,
    0,
    uX / 2,
    uY / 2,
    uX / 2
  );
  fillEmpty.addColorStop(0, "#ffffff01");
  fillEmpty.addColorStop(1, "#ffffff06");

  let cellState = -1;
  let cellConfig = -1;

  for (let x = 0; x < boardWidth; x++) {
    for (let y = 0; y < boardHeight; y++) {
      // x * uX, y * uY
      cellState = boardState[x + y * boardWidth];
      cellConfig = boardMap[x + y * boardWidth];
      switch (cellState) {
        // Pristine
        case 0:
          {
            const offsetX = uX / 10;
            const offsetY = uY / 10;
            ctx.fillStyle = "#335f";
            ctx.fillRect(0, 0, uX, uY);
            const tinyTriangle = new Path2D();
            tinyTriangle.moveTo(offsetX, offsetY);
            tinyTriangle.lineTo(offsetX + uX * 0.75, offsetY + 1);
            tinyTriangle.lineTo(offsetX + 1, offsetY + uY * 0.75);
            ctx.fillStyle = "#cccccc0a";
            ctx.fill(tinyTriangle);
          }

          break;

        // Revealed
        case 1:
          ctx.fillStyle = fillEmpty;
          ctx.fillRect(0, 0, uX, uY);

          ctx.fillStyle = "#fffa";
          switch (cellConfig) {
            // Revealed & empty
            case 0:
              break;

            // Revealed & bomb
            case 9:
              ctx.fillText(
                status === GameStatus.WON ? "🎉" : "💥" /*"💣"*/,
                uX / 3,
                uY / 1.7
              );
              break;

            // Revealed & number
            default:
              ctx.fillText(cellConfig, uX / 2.5, uY / 1.6);
              break;
          }
          break;

        // Flagged
        case 2:
          {
            const offsetX = uX / 10;
            const offsetY = uY / 10;
            ctx.fillStyle = "#335f";
            ctx.fillRect(0, 0, uX, uY);
            const tinyTriangle = new Path2D();
            tinyTriangle.moveTo(offsetX, offsetY);
            tinyTriangle.lineTo(offsetX + uX * 0.75, offsetY + 1);
            tinyTriangle.lineTo(offsetX + 1, offsetY + uY * 0.75);
            ctx.fillStyle = "#cccccc0a";
            ctx.fill(tinyTriangle);

            ctx.fillStyle = "#ffff";
            ctx.fillText(`🚩`, uX / 2.5, uY / 1.7);
          }
          break;

        default:
          ctx.fillStyle = "#f33f";
          ctx.fillText(`X`, uX / 2.5, uY / 1.7);
          break;
      }

      // DEBUG
      // ctx.fillStyle = "#fff3";
      // ctx.fillText(cellConfig, 4, 11);
      // ctx.fillText(cellState, 21, 11);
      // ctx.fillText(x + y * boardWidth, 4, 26);
      // if (cellConfig === 9) {
      //   ctx.fillText(`💣`, 18, 26);
      // }

      // console.log(`${x},${y}`);

      ctx.translate(0, uY);
    }
    ctx.translate(uX, -height);
  }

  ctx.restore();
}

export { drawBoard };
